package com.demo.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by weihuaguo on 2019/2/28 17:42.
 */
public class Message implements Serializable {

    private String millis;

    private Date date;

    public String getMillis() {
        return millis;
    }

    public void setMillis(String millis) {
        this.millis = millis;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
