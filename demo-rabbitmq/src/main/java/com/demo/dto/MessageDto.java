package com.demo.dto;

import java.io.Serializable;

/**
 * Created by weihuaguo on 2019/3/16 14:20.
 */
public class MessageDto implements Serializable {
    private Boolean ack;

    private Integer id;

    private String name;

    private String value;

    public Boolean getAck() {
        return ack;
    }

    public void setAck(Boolean ack) {
        this.ack = ack;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
