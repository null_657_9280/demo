package com.demo.rabbitmq;

import com.demo.utils.ObjectMapperUtils;
import com.rabbitmq.client.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * 接收者, 监听 hello 队列
 */
@Component
@RabbitListener(queues = "hello")
public class HelloReceiver {

    private final static Logger logger = LoggerFactory.getLogger(HelloReceiver.class);

    /**
     * 处理 队列消息, 只有处理完当前队列, 才会再处理下一个队列
     */
    @RabbitHandler
    public void processString(String message) {
        long millis = Long.parseLong(message);
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        logger.info("hello 接收到消息:{} ", message);
    }

    @RabbitHandler
    public void processObject(com.demo.dto.Message message1, Channel channel, Message message) throws IOException {
        try {
            Thread.sleep(Long.parseLong(message1.getMillis()));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        logger.info("hello 1接收到对象:{} ", ObjectMapperUtils.toString(message1));
        long deliveryTag = message.getMessageProperties().getDeliveryTag();
//        channel.basicNack(deliveryTag, false, true);      //否认消息
//        channel.basicAck(deliveryTag, false);            //确认消息
//        channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
    }

}
