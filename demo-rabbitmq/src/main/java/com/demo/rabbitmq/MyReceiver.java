package com.demo.rabbitmq;

import com.demo.dto.Message;
import com.demo.utils.ObjectMapperUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * 接收者, 监听 hello 队列
 */
@Component
public class MyReceiver {

    private final static Logger logger = LoggerFactory.getLogger(HelloSender.class);

    /**
     * 处理 队列消息, 只有处理完当前队列, 才会再处理下一个队列
     */
    @RabbitListener(queuesToDeclare = @Queue("myQueue"))
    public void processString(String message) {
        long millis = Long.parseLong(message);
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        logger.info("MyReceiver 接收到消息:{} ", message);
    }

    @RabbitListener(queuesToDeclare = @Queue("myQueue"))
    public void processObject(Message message) {
        try {
            Thread.sleep(Long.parseLong(message.getMillis()));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        logger.info("MyReceiver 接收到对象:{} ", ObjectMapperUtils.toString(message));
    }

}
