package com.demo.rabbitmq;

import com.demo.dto.Message;
import com.demo.utils.ObjectMapperUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 发送者
 */
@Component
public class HelloSender {

    private final static Logger logger = LoggerFactory.getLogger(HelloSender.class);

    @Autowired
    private AmqpTemplate template;


    /**
     * 发送 字符串消息
     */
    public void sendString(String queue, String message) {
//        message = message + "..." +LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
        logger.info("发送消息 队列:{}, 内容:{}", queue, message);
        this.template.convertAndSend(queue, message);
    }

    /**
     * 发送 对象消息
     */
    public void sendObject(String queue, String millis) {
        Message message = new Message();
        message.setMillis(millis);
        message.setDate(new Date());
        logger.info("发送对象 队列:{}, 内容:{}", queue, ObjectMapperUtils.toString(message));
        this.template.convertAndSend(queue, message);
    }

}
