package com.demo.rabbitmq;

import com.demo.dto.MessageDto;
import com.rabbitmq.client.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.LocalDateTime;

/**
 * Created by weihuaguo on 2019/3/16 14:28.
 */
@Component
public class AReceiver {
    private final static Logger logger = LoggerFactory.getLogger(AReceiver.class);

    @RabbitListener(queuesToDeclare = @Queue("aQueue"))
    public void processString(String msgString, Channel channel, Message message) {
        logger.info("processString 收到:{}, 时间:{}", msgString, LocalDateTime.now());
        ack(channel, message);
    }

    @RabbitListener(queuesToDeclare = @Queue("aQueueObj"))
    public void processObject(MessageDto messageDto, Channel channel, Message message) {
        logger.info("processObject 收到:{}, 时间:{}", messageDto, LocalDateTime.now());
        ack(channel, message);
    }

    private void ack(Channel channel, Message message) {
        try {
            //告诉服务器收到这条消息 已经被我消费了 可以在队列删掉 这样以后就不会再发了 否则消息服务器以为这条消息没处理掉 后续还会在发
            channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
            logger.info("receiver success");
//            int i = 1 / 0;
        } catch (Exception e) {
            try {
                //否认, 第三个参数 requeue: false 直接丢弃消息; true 不丢弃, 消息会循环发送过来
                channel.basicNack(message.getMessageProperties().getDeliveryTag(), false, false);
            } catch (IOException e1) {
                logger.error("channel.basicNack fail e:", e1);
            }
            logger.error("receiver fail e:", e);
        }
    }
}
