package com.demo.rabbitmq;

import com.rabbitmq.client.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.LocalDateTime;

/**
 * Created by weihuaguo on 2019/3/16 14:28.
 */
@Component
public class TopicReceiver {
    public static final String QUEUE_TYPE = "order.";
    public static final String QUEUE_1 = QUEUE_TYPE + "queue1";
    public static final String QUEUE_2 = QUEUE_TYPE + "queue2";
    public static final String EXCHANGE_ORDER = "exchangeOrder";

    private final static Logger logger = LoggerFactory.getLogger(TopicReceiver.class);

    @RabbitListener(bindings = @QueueBinding(
            exchange = @Exchange(value = EXCHANGE_ORDER, type = ExchangeTypes.TOPIC),
            key = QUEUE_1,
            value = @Queue(QUEUE_1)//队列名
    ))
    public void processString1(String messageContent, Channel channel, Message message) throws IOException {
        logger.info("1 收到:{}, 时间:{}", messageContent, LocalDateTime.now());
        long deliveryTag = message.getMessageProperties().getDeliveryTag();
        //        channel.basicNack(deliveryTag, false, true);      //否认消息
        channel.basicAck(deliveryTag, false);            //确认消息
    }

//    @RabbitListener(bindings = @QueueBinding(
//            exchange = @Exchange(EXCHANGE_ORDER),
//            key = QUEUE_2,
//            value = @Queue(QUEUE_2)//队列名
//    ))
//    public void processString2(String messageContent, Channel channel, Message message) {
//        logger.info("2 收到:{}, 时间:{}", messageContent, LocalDateTime.now());
//    }

    @RabbitListener(bindings = @QueueBinding(
            exchange = @Exchange(value = EXCHANGE_ORDER, type = ExchangeTypes.TOPIC),
            key = QUEUE_TYPE + "#",
            value = @Queue(QUEUE_2)//队列名
    ))
    public void processString_(String messageContent, Channel channel, Message message) throws IOException {
        logger.info("# 收到:{}, 时间:{}", messageContent, LocalDateTime.now());
        long deliveryTag = message.getMessageProperties().getDeliveryTag();
//        channel.basicNack(deliveryTag, false, true);      //否认消息
        channel.basicAck(deliveryTag, false);            //确认消息
    }


    /**
     * 订单延迟队列（死信队列）
     */
    @Bean
    public org.springframework.amqp.core.Queue orderTtlQueue() {
        return QueueBuilder
                .durable(QUEUE_1 + ".ttl")
                .withArgument("x-dead-letter-exchange", EXCHANGE_ORDER)//到期后转发的交换机
                .withArgument("x-dead-letter-routing-key", QUEUE_1)//到期后转发的路由键
                .build();
    }

    /**
     * 订单延迟队列队列所绑定的交换机
     */
    @Bean
    DirectExchange orderTtlDirect() {
        return (DirectExchange) ExchangeBuilder
                .directExchange(EXCHANGE_ORDER + ".ttl")
                .durable(true)
                .build();
    }

    /**
     * 将订单延迟队列绑定到交换机
     */
    @Bean
    Binding orderTtlBinding(DirectExchange orderTtlDirect, org.springframework.amqp.core.Queue orderTtlQueue){
        return BindingBuilder
                .bind(orderTtlQueue)
                .to(orderTtlDirect)
                .with(QUEUE_1 + ".ttl");
    }
}
