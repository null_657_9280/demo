package com.demo.rabbitmq;

import com.demo.dto.MessageDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

/**
 * Created by weihuaguo on 2019/3/16 14:21.
 */
@Service
public class ASender /*implements RabbitTemplate.ReturnCallback*/ {

    private final static Logger logger = LoggerFactory.getLogger(ASender.class);

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void send(Integer delayed) {
        String context = "你好现在是 " + LocalDateTime.now();
        logger.info("ASender发送内容: {}", context);
//        this.rabbitTemplate.setReturnCallback(this);
        this.rabbitTemplate.setConfirmCallback((correlationData, ack, cause) -> {
            if (ack) {
                logger.info("ASender 消息发送成功");
            } else {
                logger.info("ASender消息发送失败 cause:{}, correlationData:{}", cause, correlationData);
            }
        });
        this.rabbitTemplate.convertAndSend("aQueue", context, delayed);
    }

    public void sendObj() {
        MessageDto obj = new MessageDto();
        obj.setAck(false);
        obj.setId(123);
        obj.setName("rabbit MQ");
        obj.setValue(LocalDateTime.now().toString());
        logger.info("发送 obj: {}", obj);
        this.rabbitTemplate.convertAndSend("aQueueObj", obj);
    }


//    @Override
//    public void returnedMessage(Message message, int i, String s, String s1, String s2) {
//        logger.info("returnedMessage message: {}, i: {}, s: {}, s1: {}, s2: {}", message, i, s, s1, s2);
//    }

//    @Override
//    public void confirm(CorrelationData correlationData, boolean b, String s) {
//        System.out.println("sender success");
//    }

}
