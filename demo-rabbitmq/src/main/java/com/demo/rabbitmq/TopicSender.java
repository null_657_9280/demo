package com.demo.rabbitmq;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 发送者
 */
@Component
public class TopicSender {

    private final static Logger logger = LoggerFactory.getLogger(TopicSender.class);

    @Autowired
    private AmqpTemplate template;

    /**
     * 发送 字符串消息
     */
    public void sendString(String routingKey, String message) {
        logger.info("发送消息 队列:{}, 内容:{}", routingKey, message);
        this.template.convertAndSend(TopicReceiver.EXCHANGE_ORDER, routingKey, message);
    }

    /**
     * 发送 字符串消息, 延时发送
     */
//    public void sendString(String message, final long delayTimes) {
//        logger.info("发送消息 内容:{}", message);
//        //给延迟队列发送消息
//        template.convertAndSend(TopicReceiver.EXCHANGE_ORDER, TopicReceiver.QUEUE_1, message, message1 -> {
//            //给消息设置延迟毫秒值
//            message1.getMessageProperties().setExpiration(String.valueOf(delayTimes));
//            return message1;
//        });
//    }

    public void sendString(String message, final long delayTimes) {
        logger.info("发送消息 内容:{}", message);
        //给延迟队列发送消息
        template.convertAndSend(TopicReceiver.EXCHANGE_ORDER + ".ttl", TopicReceiver.QUEUE_1 + ".ttl", message, message1 -> {
            //给消息设置延迟毫秒值
            message1.getMessageProperties().setExpiration(String.valueOf(delayTimes));
            return message1;
        });
    }
}
