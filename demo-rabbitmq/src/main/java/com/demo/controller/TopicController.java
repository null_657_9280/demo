package com.demo.controller;

import com.demo.rabbitmq.TopicSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("topic")
@RestController
public class TopicController {

    @Autowired
    private TopicSender topicSender;

    @GetMapping("send")
    public String sendString(String queue, String message) {
        topicSender.sendString(queue, message);
        return "ok";
    }

    /**
     * 延时发送
     */
    @GetMapping("delayed")
    public String sendString(String message, long delayTimes) {
        topicSender.sendString(message, delayTimes);
        return "ok";
    }
}
