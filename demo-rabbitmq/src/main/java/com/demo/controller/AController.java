package com.demo.controller;

import com.demo.rabbitmq.ASender;
import com.demo.rabbitmq.HelloSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("a")
@RestController
public class AController {

    @Autowired
    private ASender aSender;

    @GetMapping("send/string")
    public String sendString(Integer delayed) {
        aSender.send(delayed);
        return "ok";
    }

    @GetMapping("send/object")
    public String sendObject() {
        aSender.sendObj();
        return "ok";
    }

}
