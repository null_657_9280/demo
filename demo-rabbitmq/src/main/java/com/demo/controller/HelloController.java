package com.demo.controller;

import com.demo.rabbitmq.HelloSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("hello")
@RestController
public class HelloController {

    @Autowired
    private HelloSender helloSender;

    @GetMapping("send/string")
    public String sendString(String queue, String message) {
        helloSender.sendString(queue, message);
        return "ok";
    }

    @GetMapping("send/object")
    public String sendObject(String queue, String millis) {
        helloSender.sendObject(queue, millis);
        return "ok";
    }

}
