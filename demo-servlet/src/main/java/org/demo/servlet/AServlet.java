package org.demo.servlet;

import cn.hutool.extra.servlet.ServletUtil;
import cn.hutool.http.ContentType;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = {"/aServlet"})
public class AServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        String runMode = System.getProperty("run.mode");
        System.out.println("runMode=" + runMode);
        runMode = runMode == null ? "null" : runMode;
        ServletUtil.write(resp, runMode, ContentType.JSON.toString());
    }
}
