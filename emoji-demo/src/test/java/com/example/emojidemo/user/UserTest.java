package com.example.emojidemo.user;

import com.example.emojidemo.entity.User;
import com.example.emojidemo.entity.UserInfo;
import com.example.emojidemo.enums.user.State;
import com.example.emojidemo.mapper.UserMapper;
import com.example.emojidemo.repository.UserRepository;
import com.example.emojidemo.utils.ObjectMapperUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by weihuaguo on 2018/12/5 16:12.
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserMapper userMapper;

    @Test
    public void save() {
        User user = new User();
        user.setUsername("David");
        userRepository.save(user);
    }

    @Test
    public void save2() {
        User user = new User();
        user.setUsername(RandomStringUtils.randomAlphabetic(6));
        user.setState(State.NORMAL);
        userMapper.insert(user);
    }

    @Test
    public void save3() {
//        UserInfo userInfo = new UserInfo().selectById(1082941172474707968L);
//        System.out.println(userInfo);
    }
    @Test
    public void get() {
        User user = userMapper.selectById(1082938951389642753L);
        System.out.println(ObjectMapperUtils.toString(user));
    }

}
