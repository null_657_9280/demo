package com.example.emojidemo.test;

import com.example.emojidemo.entity.Address;
import com.example.emojidemo.service.AddressService;
import com.example.emojidemo.utils.ObjectMapperUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * Created by weihuaguo on 2018/12/25 10:10.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class AddressTest {

    @Autowired
    private AddressService addressService;

    @Test
    public void test1() {
        Address address = addressService.getById(1);
        System.out.println(ObjectMapperUtils.toString(address));
    }

    @Test
    public void test2() {
        List<Address> addressList = addressService.getByPid(1);
        addressList.forEach(address -> {
            System.out.println(ObjectMapperUtils.toString(address));
        });

    }

}
