package com.example.emojidemo.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

/**
 * Created by weihuaguo on 2018/12/25 10:10.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class RedisTest {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Test
    public void test1() {
        stringRedisTemplate.opsForValue().set("key:a", "aaa");
    }

    private static final String SIGN_IN_666 = "signIn:666";


    @Test
    public void test2() {
        for (int i = 0; i < 20; i++) {
            //左边插入 上边 会排 前面
//            stringRedisTemplate.opsForList().leftPush(key, String.valueOf(i));

            //右边插入 下边 会排 后面
            stringRedisTemplate.opsForList().rightPush(SIGN_IN_666, String.valueOf(i));
        }
    }

    @Test
    public void test3() {
        // 通过索引获取 索引从前面开始
        String index = stringRedisTemplate.opsForList().index(SIGN_IN_666, 0);
        System.out.println(index);
    }

    private static final String SIGN_IN_667 = "signIn:667";
    private static final String SIGN_IN_WEEK = "signIn:week";
    private static final List<String> SIG_IN_LIST = Arrays.asList("20181220","20181221","20181223","20181224","20181225","20181226");
    private static final List<String> SIG_IN_WEEK = Arrays.asList("20181220","20181221","20181222","20181223","20181224","20181225","20181226");

    @Test
    public void test4() {
        stringRedisTemplate.opsForSet().add(SIGN_IN_667, SIG_IN_LIST.toArray(new String[]{}));
        stringRedisTemplate.opsForSet().add(SIGN_IN_WEEK, SIG_IN_WEEK.toArray(new String[]{}));
//        SINTER key1 [key2]
//        返回给定所有集合的交集
        Set<String> difference = stringRedisTemplate.opsForSet().intersect(SIGN_IN_667, SIGN_IN_WEEK);
        System.out.println(difference.size());
        for (String s : difference) {
            System.out.println(s);
        }
    }
}
