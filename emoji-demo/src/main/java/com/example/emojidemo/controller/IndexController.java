package com.example.emojidemo.controller;

import com.example.emojidemo.entity.User;
import com.example.emojidemo.entity.UserInfo;
import com.example.emojidemo.mapper.UserInfoMapper;
import com.example.emojidemo.repository.UserRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

/**
 * Created by weihuaguo on 2018/12/5 16:01.
 */
@RequestMapping("index")
@Controller
public class IndexController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserInfoMapper userInfoMapper;

    @GetMapping("index")
    public String index() {
        return "index";
    }

    @ResponseBody
    @PostMapping("save")
    public String save(User user) {
        userRepository.save(user);
        return "ok";
    }

    @ResponseBody
    @PostMapping("save2")
    public String save2(@RequestBody User user) {
        userRepository.save(user);
        return "ok";
    }

    @ResponseBody
    @GetMapping("get")
    public User get(Long id) throws JsonProcessingException {
//        ObjectMapper objectMapper = new ObjectMapper();
//        objectMapper.writeValueAsString(null);
        Optional<User> byId = userRepository.findById(id);
        return byId.get();
    }

    @ResponseBody
    @GetMapping("get2")
    public UserInfo get2(Long id){
        return userInfoMapper.selectById(id);
//        return new UserInfo().selectById(id);
    }

    @GetMapping("weixin")
    public String weixin() {
        return "weixin";
    }

    @ResponseBody
    @GetMapping("wxRedirect")
    public String wxRedirect() {
        return "weixin";
    }

}
