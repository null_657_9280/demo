package com.example.emojidemo.controller;

import com.example.emojidemo.entity.Address;
import com.example.emojidemo.service.AddressService;
import com.example.emojidemo.service.UserService;
import com.example.emojidemo.utils.ObjectMapperUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by weihuaguo on 2018/12/23 14:18.
 */
@RestController
@RequestMapping("cach")
public class CachController {

    @Autowired
    private UserService userService;

    @Autowired
    private AddressService addressService;

    @GetMapping("getUser/{id}")
    public String getUser(@PathVariable Long id) {
        return ObjectMapperUtils.toString(userService.getUser(id));
    }

    @GetMapping("getUserAll")
    public String getUserAll() {
        return ObjectMapperUtils.toString(userService.getAll());
    }


    @GetMapping("getAddress/{id}")
    public Address getAddress(@PathVariable Integer id) {
        return addressService.getById(id);
    }

    @GetMapping("getAddressPid/{id}")
    public List<Address> getAddressPid(@PathVariable Integer id) {
        return addressService.getByPid(id);
    }

    @GetMapping("addressEvict")
    public String addressEvict() {
        addressService.evict();
        return "ok";
    }

    @GetMapping("concurrentMapCache")
    public String concurrentMapCache() {
        addressService.getMapCache();
        return "ok";
    }

}
