package com.example.emojidemo.controller;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.sql.Timestamp;


@RequestMapping("app/wenjuan")
@Controller
public class WjController {

    private static final Logger logger = LoggerFactory.getLogger(WjController.class);

    @GetMapping("submit")
    public String submit(HttpServletRequest request)  {
        return "wenjuan";
    }

    @ResponseBody
    @PostMapping("push")
    public String get(HttpServletRequest request) throws IOException {
        String jsonString = IOUtils.toString(request.getInputStream());
        System.out.println(jsonString);
        logger.info(jsonString);
        return "ok";
    }

    @ResponseBody
    @GetMapping("timestamp")
    public String timestamp(Timestamp timestamp)  {
        System.out.println(timestamp);
        return timestamp.toString();
    }

    @ResponseBody
    @PostMapping("h5tj")
    public String h5tj(HttpServletRequest request) throws IOException {
        return "ok";
    }
}
