package com.example.emojidemo.controller;

import com.example.emojidemo.entity.UserInfo;
import com.example.emojidemo.mapper.UserInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by weihuaguo on 2019/1/10 9:58.
 */
@RequestMapping("user")
@RestController
public class UserController {

    @Autowired
    private UserInfoMapper userInfoMapper;

    @GetMapping("get/{id}")
    public UserInfo get(@PathVariable long id) {
        return userInfoMapper.selectById(id);
    }
}
