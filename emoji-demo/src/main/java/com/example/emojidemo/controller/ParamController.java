package com.example.emojidemo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * Created by weihuaguo on 2018/12/7 9:55.
 */
@RestController
@RequestMapping("param")
public class ParamController {

    @RequestMapping(value = "a", produces = "text/plain;charset=UTF-8")
    public String a(@RequestParam("param") Map<String, Object> param) {
        return param.toString();
    }


    @RequestMapping(value = "b", produces = "text/plain;charset=UTF-8")
    public String b(@RequestParam("a") String param) {
        return param;
    }

}
