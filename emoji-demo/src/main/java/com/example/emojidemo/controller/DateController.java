package com.example.emojidemo.controller;

import org.apache.commons.lang3.time.DateParser;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * Created by weihuaguo on 2018/12/21 16:21.
 */
@RestController
@RequestMapping("date")
public class DateController {

    @RequestMapping("a")
    public String a(Date date) {
        return "ok";
    }

    @RequestMapping("b")
    public String b(Date date) {

        return "ok";
    }

}
