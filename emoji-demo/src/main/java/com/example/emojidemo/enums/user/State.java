package com.example.emojidemo.enums.user;

import com.baomidou.mybatisplus.core.enums.IEnum;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Created by weihuaguo on 2019/1/9 17:43.
 */
public enum State implements IEnum<Integer> {

    NORMAL(1, "正常"),
    FROZEN(-1, "冻结");

    private int value;
    private String description;

    State(int value, String description) {
        this.value = value;
        this.description = description;
    }

    @Override
    public Integer getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @JsonValue
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}