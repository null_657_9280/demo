package com.example.emojidemo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.example.emojidemo.enums.user.State;

import javax.persistence.*;

/**
 * Created by weihuaguo on 2018/12/5 16:03.
 */
@TableName("t_user")
@Entity
@Table(name = "t_user")
public class User {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;

    private String username;

    private State state;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }
}
