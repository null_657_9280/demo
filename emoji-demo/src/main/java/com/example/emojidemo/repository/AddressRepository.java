package com.example.emojidemo.repository;

import com.example.emojidemo.entity.Address;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import java.util.List;

/**
 * Created by weihuaguo on 2018/12/5 16:11.
 */
public interface AddressRepository extends CrudRepository<Address, Integer>, QueryByExampleExecutor<Address> {

    List<Address> findAllByPid(Integer pid);
}
