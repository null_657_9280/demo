package com.example.emojidemo.repository;

import com.example.emojidemo.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

/**
 * Created by weihuaguo on 2018/12/5 16:11.
 */
public interface UserRepository extends CrudRepository<User, Long>, QueryByExampleExecutor<User> {

}
