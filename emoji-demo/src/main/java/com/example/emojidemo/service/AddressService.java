package com.example.emojidemo.service;

import com.example.emojidemo.entity.Address;
import com.example.emojidemo.entity.User;
import com.example.emojidemo.repository.AddressRepository;
import com.example.emojidemo.repository.UserRepository;
import com.example.emojidemo.utils.ObjectMapperUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;

import java.util.List;


@CacheConfig(cacheNames = AddressService.cacheNames)
@Service
public class AddressService {
    static final String cacheNames = "addressCache";

    @Autowired
    private AddressRepository addressRepository;

    @Cacheable(key = "#id")
    public Address getById(Integer id) {
        return addressRepository.findById(id).get();
    }

    @Cacheable(key = "#pid")
    public List<Address> getByPid(Integer pid) {
        return addressRepository.findAllByPid(pid);
    }

    public void getMapCache() {
        ConcurrentMapCache concurrentMapCache = new ConcurrentMapCache(cacheNames);
        String name = concurrentMapCache.getName();
        System.out.println("name = " + name);
        System.out.println(ObjectMapperUtils.toString(concurrentMapCache));
        concurrentMapCache.clear();
        concurrentMapCache.evict(cacheNames);

    }

    @CacheEvict(allEntries = true)
    public void evict() {

    }
}
