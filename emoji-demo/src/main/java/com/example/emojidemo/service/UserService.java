package com.example.emojidemo.service;

import com.example.emojidemo.entity.User;
import com.example.emojidemo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * Created by weihuaguo on 2018/12/23 14:19.
 */
@CacheConfig(cacheNames = "userCache")
@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Cacheable(key = "#id")
    public User getUser(Long id) {
        return userRepository.findById(id).get();
    }

    @Cacheable(/*condition = "#result == 4"*/)
    public Iterable<User> getAll() {
        Iterable<User> all = userRepository.findAll();
        return all;
    }
}
