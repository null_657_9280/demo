package com.example.emojidemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@EnableCaching
@SpringBootApplication
public class EmojiDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(EmojiDemoApplication.class, args);
    }
}
