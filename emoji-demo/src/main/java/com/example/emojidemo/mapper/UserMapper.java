package com.example.emojidemo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.emojidemo.entity.User;


public interface UserMapper extends BaseMapper<User> {
}
