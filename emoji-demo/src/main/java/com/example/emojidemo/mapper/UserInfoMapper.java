package com.example.emojidemo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.emojidemo.entity.UserInfo;


public interface UserInfoMapper extends BaseMapper<UserInfo> {
}
