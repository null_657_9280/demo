package com.example.postgresql.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.postgresql.entity.User;

/**
 * Created by weihuaguo on 2019/2/17 19:29.
 */
public interface UserMapper extends BaseMapper<User> {
}
