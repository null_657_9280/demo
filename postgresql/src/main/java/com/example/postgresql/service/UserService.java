package com.example.postgresql.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.postgresql.entity.User;
import com.example.postgresql.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

/**
 * Created by weihuaguo on 2019/2/17 19:29.
 */
@Service
public class UserService extends ServiceImpl<UserMapper, User> {


    @Autowired
    private Environment environment;


    @PostConstruct
    public void init() {
        String[] activeProfiles = environment.getActiveProfiles();
        if (activeProfiles != null && activeProfiles.length > 0) {
            String activeProfile = activeProfiles[0];
            System.out.println("\n==========================================activeProfile:" + activeProfile + "====================\n");
        }
        System.out.println("\n==========================================environment:" + environment + "====================\n");
    }
}
