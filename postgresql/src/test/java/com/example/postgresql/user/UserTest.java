package com.example.postgresql.user;

import cn.hutool.json.JSONUtil;
import com.example.postgresql.entity.User;
import com.example.postgresql.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

/**
 * Created by weihuaguo on 2019/2/17 19:31.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserTest {

    @Autowired
    private UserService userService;

    @Test
    public void test1() {
        User byId = userService.getById(1L);
        System.out.println(JSONUtil.toJsonStr(byId));
    }

    @Test
    public void insert() {
        User user = new User();
        user.setMobile("17612075716");
        user.setNickname("库的");
        user.setCreateTime(new Date());
        boolean save = userService.save(user);
        System.out.println(save);
    }

}
